# Constructive Siteswap

[https://gab_.gitlab.io/constructive-siteswap](https://gab_.gitlab.io/constructive-siteswap)


## Installation

Copy these files from web_build to the static web server:
  * index.html
  * bundle.js
  * graphic files (\*.svg, \*.ttf, \*.woff)
  
## Development

### Requirement

* bundler (ruby package manager)
* npm (javascript package manager)
* webpack (javascript bundler)
* ...

### Bundle from sources

1. Run `rake bundle` to load ruby and js dependencies, compile and bundle
2. Run `rake webpack_dev` to start a web server with auto-reload
3. Open [http://localhost:8080](http://localhost:8080) and see the console


## Explanation

### `rake webpack`

`opalrb-loader` rely on `OPAL_LOAD_PATH` environment variable to correctly resolve file other than relative path.

This task simply supply the variable to webpack by using `Opal.paths`. Gem which is aware of Opal will correctly add its path to `Opal.paths` on required. Otherwise, you need to invoke `Opal.use_gem` to manually collect paths.
