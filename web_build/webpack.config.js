var path = require("path")

module.exports = {
  entry: ["./entry.js", './entry.rb'],
  output: {
    filename: 'bundle.js'       
  },
  module: {
    loaders: [
      {
        test: /\.rb$/,
        loader: "opalrb-loader",
        // query: {
        //   requirable: false,
        //   freezing: false
        // }
      },
      {
        test: /\.css$/,
        loader: "style!css"
      },
      { 
        test: /\.png$/, 
        loader: "url-loader?limit=100000" 
      },
      { 
        test: /\.jpg$/, 
        loader: "file-loader" 
      },
      {
        test: /\.(woff|woff2)(\?v=\d+\.\d+\.\d+)?$/, 
        loader: 'url?limit=10000&mimetype=application/font-woff'
      },
      {
        test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/, 
        loader: 'url?limit=10000&mimetype=application/octet-stream'
      },
      {
        test: /\.eot(\?v=\d+\.\d+\.\d+)?$/, 
        loader: 'file'
      },
      {
        test: /\.svg(\?v=\d+\.\d+\.\d+)?$/, 
        loader: 'url?limit=10000&mimetype=image/svg+xml'
      }
    ]
  }
};