

require 'readline'
require_relative '../lib/constructive_siteswap/filled_array'
require_relative '../lib/constructive_siteswap/juggler'
require_relative '../lib/constructive_siteswap/siteswap_parser'


class CLI

  include SiteswapParser

  def initialize( hands: 2, balls: 3, max_height: 5, max_multi: 3,
                  parse: true, default_height: 1, to_hand: nil)
    @juggler        = Juggler.new(hands: hands, balls: balls)
    @max_height     = max_height
    @max_multi      = max_multi
    @parse          = parse
    @default_height = default_height.zero? ? nil : default_height
    @to_hand        = to_hand
    @siteswap       = ''
  end

  def run!
    trap('INT') { puts ; exit }
    loop do
      throw = @parse ? read_throw : read_and_throw_by_path!
      @siteswap += throw_to_ss(throw, @juggler.in_hands)
      @parse ? @juggler.throw!(throw) : @juggler.catch!
      puts '-' * Readline.get_screen_size.last
    end
  end

  # Reads throw until it can be parsed
  def read_throw
    print_state
    loop do
      ss_throw = Readline.readline("Throw: ", true)
      puts unless ss_throw
      begin
        return parse_ss_throw(
          ss_throw, @juggler.in_hands,
          @default_height, @juggler.current_hand, @to_hand
        )
      rescue SSParsingError => err
        puts err
        next
      end
    end
  end

  # Reads throw path for each ball and throws it
  def read_and_throw_by_path!
    @states.flat_map.with_index do |_, from_hand|
      @in_hands[from_hand].times.map do
        print_state
        prompt  = "Throw from hand #{from_hand} to hand: "
        err     = "This hand doesn't exist, please try again."
        to_hand = readline_until(prompt, err){|input| @juggler.states[input.to_i] }.to_i
        prompt  = "With height: "
        err     = "Height must be > 0, please try again."
        height  = readline_until(prompt, err){|input| input.to_i > 0 }.to_i
        throw   = {height: height, from_hand: from_hand, to_hand: to_hand}
        @juggler.throw_one!(throw)
        throw
      end
    end
  end

  # Reads until block returns true with the entry and puts error string if not
  def readline_until(prompt = "", error = nil)
    loop do
      input = Readline.readline(prompt, true)
      puts unless input
      return input if yield input
      puts error if error
    end
  end

  # Puts the juggler state and which throws are possible at this time
  def print_state
    h_states = @juggler.states.map.with_index{|state, i| "Hand #{i}: #{state.join}" }
    to_throw = @juggler.in_hands.map.with_index{|in_hand, i|
      "#{'*' if i == @juggler.current_hand}Hand #{i}: #{in_hand}" }
    poss_van = possible_catches.map.with_index{|poss, i| "Hand #{i}: #{poss.join(', ')}" }
    poss_mul = possible_catches(:multi).map.with_index{|poss, i| "Hand #{i}: #{poss.join(', ')}" }
    puts "States: #{h_states.join('   ')}"
    puts "Balls to throw: #{to_throw.join('   ')}"
    puts "Siteswap: #{@siteswap}"
    puts "Possible catches:\nvanilia: #{poss_van.join('   ')}\nmultiplex: #{poss_mul.join('   ')}"
  end

  # Returns possible catches by hand 
  def possible_catches(arg = nil)
    @juggler.states.map.with_index do |state, i|
      (1..@max_height).select do |h|
        t = state[h-1]
        if arg == :multi
          t > 0 && t < @max_multi
        else
          t <= 0
        end
      end
    end
  end
  
end
